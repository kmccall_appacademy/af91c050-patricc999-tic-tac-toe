class ComputerPlayer

  attr_reader :name

  attr_accessor :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    available_moves = []

    [0, 1, 2].each do |row|
      [0, 1, 2].each do |column|
        if board.empty?([row, column])
          available_moves.push([row, column])
        end
      end
    end

    available_moves.each do |move|
      board.place_mark(move, mark)
      if board.winner
        board.place_mark(move, nil)
        return move
      else
        board.place_mark(move, nil)
      end
    end

    available_moves.shuffle.first

  end

end
