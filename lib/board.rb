class Board

  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    if self[pos] == nil
      true
    else
      false
    end
  end

  def winner
    if !diaganal_win.nil?
      diaganal_win
    elsif !row_win.nil?
      row_win
    elsif !column_win.nil?
      column_win
    end
  end

  def diaganal_win
    if grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]
      grid[0][0]
    elsif grid[0][2] == grid[1][1] && grid[1][1] == grid[2][0]
      grid[0][2]
    end
  end

  def row_win
    grid.each do |row|
      if row.uniq == [:X]
        return :X
      elsif row.uniq == [:O]
        return :O
      end
    end
    nil
  end

  def column_win
    grid.transpose.each do |column|
      if column.uniq == [:X]
        return :X
      elsif column.uniq == [:O]
        return :O
      end
    end
    nil
  end

  def over?
    grid.flatten.none? { |mark| mark == nil } || winner
  end

end
