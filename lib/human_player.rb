class HumanPlayer

  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts 'Where woud you like to mark?'
    move = gets.chomp
    move.split(' ').map(&:to_i)
  end

  def display(board)
    puts board.grid
  end

end
